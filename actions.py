# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/core/actions/#custom-actions/


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List
#
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
#
#
# class ActionHelloWorld(Action):
#
#     def name(self) -> Text:
#         return "action_hello_world"
#
#     def run(self, dispatcher: CollectingDispatcher,
#             tracker: Tracker,
#             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
#
#         dispatcher.utter_message(text="Hello World!")
#
#         return []

class ActionVehicleType(Action):

    def name(self) -> Text:
        return "action_vehicle_type"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
            
        entities = tracker.latest_message['entities']
        
        for e in entities:
            if e['entity'] == 'vehicle':
                name = e['value']
                
            if name == "car" or name =="Car" :
                dispatcher.utter_message("We have  1. Toyota Corolla Rs.1000/day  2. Hyundai i10 Rs.600/day  3. Hyundai Verna Rs.1000/day")
                
            if name =="jeep" or name =="Jeep":
                dispatcher.utter_message("We have  1. Scorpio Rs.3000/day  2. Toyota Land Cruiser Rs.3500/day")
                
            if name =="Van" or name =="van":
                dispatcher.utter_message("We have  1. Toyota Jumbo Hiace Rs.5000/day")
                
        return []
        
        

