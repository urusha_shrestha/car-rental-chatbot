## car_registration
* greet
  - utter_welcome
  - utter_vehicles
  - utter_type
* vehicle_type
  - action_vehicle_type
  - utter_vehicle_name
*vehicle_name
  - utter_pick_up_location
* pick_up_location
  - utter_drop_location
* drop_location
  - utter_rent_date
* rent_date
  - utter_return_date
* return_date
  - utter_confirm
* confirm
  - utter_confirmed
* goodbye
  - utter_goodbye

## New Story

* greet
    - utter_welcome
    - utter_vehicles
    - utter_type
* vehicle_type{"vehicle":"car"}
    - action_vehicle_type
* vehicle_name
    - utter_pick_up_location
* pick_up_location{"pickUpLocation":"Kathamndu"}
    - utter_drop_location
* drop_location{"dropLocation":"Kathmandu"}
    - utter_rent_date
* rent_date{"rentDate":"10/10/2020 11:00 AM"}
    - utter_return_date
* return_date{"returnDate":"12/10/2020 9:00 AM"}
    - utter_confirm
* confirm
    - utter_confirmed
* goodbye
    - utter_goodbye

## New Story

    - utter_welcome
* greet
    - action_vehicle_type
* vehicle_name
    - utter_pick_up_location
* pick_up_location{"pickUpLocation":"Kathmandu"}
    - utter_drop_location
* drop_location{"dropLocation":"Bhaktapur"}
    - utter_rent_date
* rent_date{"rentDate":"Rent Date","returnDate":"10/13/2020 11:53 AM"}
    - utter_return_date
* return_date{"returnDate":"12/12/2020 11:00AM"}
    - utter_confirm
* confirm
    - utter_confirmed
* goodbye
    - utter_goodbye

## New Story

* greet
    - utter_welcome
    - utter_vehicles
    - utter_type
* vehicle_type{"vehicle":"Jeep"}
    - action_vehicle_type
* vehicle_name
    - utter_pick_up_location
* pick_up_location{"pickUpLocation":"Lalitpur"}
    - utter_drop_location
* drop_location{"dropLocation":"Lalitpur"}
    - utter_rent_date
* rent_date{"rentDate":"Rent Date","returnDate":"13/10/2020 12:02 PM"}
    - utter_return_date
* return_date{"returnDate":"24/10/2020 11:00 AM"}
    - utter_confirm
* confirm
    - utter_confirmed
* goodbye
    - utter_goodbye

## New Story

* greet
    - utter_welcome
    - utter_vehicles
    - utter_type
* vehicle_type{"vehicle":"car"}
    - action_vehicle_type
    - utter_vehicle_name
* vehicle_name
    - utter_pick_up_location
* pick_up_location{"pickUpLocation":"Bhaktapur"}
    - utter_drop_location
* drop_location{"dropLocation":"Lalitpur"}
    - utter_rent_date
* rent_date{"rentDate":"Rent Date","returnDate":"13/10/2020 12:13 PM"}
    - utter_return_date
* return_date{"returnDate":"22/10/2020 6:00PM"}
    - utter_confirm
* confirm
    - utter_confirmed
* goodbye
    - utter_goodbye
