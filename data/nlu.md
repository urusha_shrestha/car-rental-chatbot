## intent:vehicle_type
- I would like to rent a [Jeep](vehicle).
- I would like to rent a [car](vehicle).
- I would like to rent a [van](vehicle)
- [Car](vehicle)
- [Jeep](vehicle)
- [Van](vehicle)
- [car](vehicle)

## intent:greet
- hi
- hello
- hey

## intent:vehicle_name
- Toyota Corolla
- Hyundai i10
- Hyundai Verna
- Scorpio
- Toyota Land Cruiser
- Toyota Jumbo Hiace

## intent:pick_up_location
- Pick Up: [Kathmandu](pickUpLocation)
- Pick Up: [Lalitpur](pickUpLocation)
- Pick Up: [Bhaktapur](pickUpLocation)


## intent:drop_location
- Drop: [Bhaktapur](dropLocation)
- Drop: [Kathmandu](dropLocation)
- Drop: [Lalitpur](dropLocation)

## intent:rent_date
- Rent Date: [12:10:2020 12:05 PM](rentDate)
- Rent Date: [09:09:2020 11:55 AM](rentDate)
- Rent Date: [10/10/2020 11:00 AM](rentDate)

## intent:return_date
- Return Date: [05:11:2020 04:05 PM](returnDate)
- Return Date: [09:12:2020 06:44 PM](returnDate)
- Return Date: [12/10/2020 9:00 AM](returnDate)

## intent:confirm
- Yes
- yes

## intent:goodbye
- thanks
- Thanks
